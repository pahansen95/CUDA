#include <cooperative_groups.h>

typedef unsigned long long int uint64;
typedef unsigned char uchar;
typedef unsigned int uint;

#ifdef __cplusplus
extern "C" {
#endif

void __declspec ( dllexport ) kernel_rangeValid(uint blocks, uint threads,
												uint64 kStart, uint64 kEnd, uchar nodes, uchar edges, uchar kSize, 
												uint64 *glob, uint64 globCap, uint64 globLength,
												uint64 *scraps, uint64 scrapsLength,
												uchar exitFlag, uint64 exitK );

#ifdef __cplusplus
}
#endif


using namespace cooperative_groups;

const uint CAP = 30000/8;

__global__ void rangeValid( uint64 kStart, uint64 kEnd, uchar nodes, uchar edges, uchar kSize, 
							uint64 *glob, uint64 globCap, uint64 globLength,
							uint64 *scraps, uint64 scrapsLength,
							uchar exitFlag, uint64 exitK ) {
								
	uint64 curK, kOffset;
	uchar row, col, rowCount, kBitLocation, kMirrorBitLocation, i, j, threadFlag = 0;
	uint temp;
	__shared__ uint64 tempSol[CAP];//30kb of shared memory per block; 30000 bytes / 8btyes per value
	__shared__ uint length, threadCount;
	__shared__ uint sharedCopy;
	
	/*For Descriptive purposes only, these variables will be unified memory defined from outside the kernel
	extern __global__ uint64 glob[], scraps[];
	extern __global__ unsinged int globLength, scrapsLength;
	extern __global__ uchar exitFlag;
	extern __global__ uint64 exitK; //initially 0
	*/
	
	
	//cooperative_groups
	thread_block block = this_thread_block();
	thread_block_tile<32> warpTile = tiled_partition<32>(block); //break up threads into groups of warpTiles
	//coalesced_group active;
	
	//initialize shared values
	if (block.thread_rank() == 0){
		length = 0;
		threadCount = 0;
		sharedCopy = 0;
	}
	block.sync();
	
	curK = blockIdx.x*blockDim.x + threadIdx.x + kStart;
	kOffset = blockDim.x * gridDim.x;
	while (curK <= kEnd) {
		//active = NULL; //reset active threads
		
		kBitLocation = 1;//assuming the first bit in the kvalue has a position 1;
		//validate graph solution
		for (row = 0; row < nodes; row++) {//for each row
			rowCount = 0;
			kMirrorBitLocation = row;//the bit position for the mirrored kvals is always starts at the row value (assuming the first row has a position of 0)
			for (col = 0; col < nodes; col++) {//for each column in the current row (ie. a single place)
				if (col > row) {
					if (curK & (uint64(1) << (kSize - kBitLocation))) {//add one to kIterator to convert to counting space
						rowCount++;
					}
					kBitLocation++;
				}
				if (col < row) {
					if (col > 0) {
						kMirrorBitLocation += (nodes - 2) - (col - 1);
					}
					if (curK & (uint64(1) << (kSize - kMirrorBitLocation))) {//if bit is set
						rowCount++;
					}
				}
			}
			if (rowCount != edges) {
				//set the ith bit to zero
				goto endWhileLoop;
			}
		}
		
		atomicAdd(&threadCount, 1); //value found
		threadFlag = 1;
	
		endWhileLoop: //used to break out of the for loop
		
		warpTile.sync();		
		if (warpTile.thread_rank()==0 && threadCount + length > CAP) { //shared array needs to be copied to global array
			atomicExch(&((unsigned int)sharedCopy),1);
			
		}
		
		warpTile.sync();
		
		if (sharedCopy){ //need to copy over shared array to global array
			block.sync();
			if (block.thread_rank() == 0){ //let first thread copy over shared array to global array
				//check if global array will overflow; how to synchronize across blocks?
				temp = atomicAdd(&globLength, length);//add the shared array length to the globLength; temp is now the starting position of the global array
				for (i=0; i < length; i ++) { //loop over shared array
					//temp + i is the position in the global array
					if (temp + i >= globCap) {//global array overflow; instead should I stop when global array is x% full to allow any leftovers to be placed in the array? Or should I have a "leftover" array to catch anything remaining.
						exitFlag = 1;
						atomicMax(&((unsigned long long int)exitK), (unsigned long long int)curK); //record the last value to be computed
						temp = atomicAdd(&scrapsLength, length-i+1);//increase the scraps length, temp is now the starting scrap position
						for (j = 0; j < length-i+1; j ++){ //dump the remaining solutions
								scraps[temp + j] = tempSol[i + j];
						}
						break; //break from the for loop
					} else {
						glob[temp+i] = tempSol[i];
					}
				}
				length = 0;
				sharedCopy = 0;
			}
			block.sync(); //wait for first thread to catch up		
			if (exitFlag){ //should I quit
				if (threadFlag){ //write last value
					temp = atomicAdd(&scrapsLength,1);//every thread gets its own position in the array
					scraps[temp] = curK;
				}
				atomicMax(&((unsigned long long int)exitK), (unsigned long long int)(curK-kOffset)); //record the last value to be computed
				return;
			}
		}
								
		if (threadFlag){ //value found
			temp = atomicAdd(&length,1);//every thread gets its own position in the array
			tempSol[temp] = curK;
		}
		
		warpTile.sync();
		threadFlag = 0;
		curK += kOffset;
	}//END While Loop
	
	//transport over last values from shared array
	if (block.thread_rank() == 0) {
		temp = atomicAdd(&scrapsLength, length);
		for (i=0; i < length; i ++) {
			scraps[temp + i] = tempSol[i];
		}
	}
	atomicMax(&((unsigned long long int)exitK), (unsigned long long int)(curK-kOffset)); //record the last value to be computed
	
	return;
}

void kernel_rangeValid( uint blocks, uint threads,
						uint64 kStart, uint64 kEnd, uchar nodes, uchar edges, uchar kSize, 
						uint64 *glob, uint64 globCap, uint64 globLength,
						uint64 *scraps, uint64 scrapsLength,
						uchar exitFlag, uint64 exitK){
							
	rangeValid<<<blocks, threads>>>(kStart, kEnd, nodes, edges, kSize, glob, globCap, globLength, scraps, scrapsLength, exitFlag, exitK);
	
}