/*
This file includes all the definitions of the functions found in the header file of the first include statement.
This file DOES NOT contain definitions for CUDA Kernels; It only calls them.
*/
//#pragma once //included for intellisense

//OS Dependant Headers
#include <Windows.h> // this needs to come first
#include <fileAPI.h>//CreateDirectory,FindFirstFile,GetFileAttributes
//Self Defined Headers
#include "graph_cuda.cuh"
#include "graph_cuda_static.cuh"
#include "graph_kernels.cuh"
//C Headers
#include <stdlib.h>
#include <stdio.h>
#include <direct.h>
#include <math.h>

#define DEVICE 0

/*
PathFileExists is a helper function for the Windows API. Given a path it returns 1 if it exists and 0 if it doesn't
*/
static int PathFileExists(char * path) {
	DWORD temp = GetFileAttributes(path);
	if (temp == INVALID_FILE_ATTRIBUTES) {
		return 0;
	}
	return 1;
}

unsigned long long int solveGraph(int nodes, int edges, char * rootPath) {
	//declerations
	char newPath[500], filePath[500], updatePath[500], host_flag = 0, *device_flag;
	cudaDeviceProp gpuProp;
	unsigned long long int usableMem, globMemUsed, i, j, solutionCount=0, count;
	struct storage *kInfo_host;
	glob *device_glob, *host_glob;
	ktype *device_storage, *host_storage;
	FILE *update;

	//update log
	sprintf(updatePath, "%s\\logs\\update.log", rootPath);//temporary
	//output file
	sprintf(newPath, "%s\\temp_results\\%d_%d", rootPath,nodes,edges);//path of folder
	//create folder structure for this combination
#ifdef _WIN64
	if (!PathFileExists(newPath)) {
		if (!CreateDirectory(newPath, NULL)) {
			fprintf(libLogFile, "ERROR: Unable to create directory in solveGraph. Exiting program");
			fflush(libLogFile);
			exit(-1);//implement your own exit function which closes all files
		}
	}
//#elif __linux__
#endif

	logCudaError(cudaSetDevice(DEVICE),"Setting Device");
	logCudaError(cudaGetDeviceProperties(&gpuProp, DEVICE), "Get Dev Properties");
	fprintf(libLogFile,"Device in Use: %s with Compute Capability %d.%d\n", gpuProp.name, gpuProp.major, gpuProp.minor);
	fflush(libLogFile);
	fprintf(libLogFile, "%d Blocks & %d Threads per Block", B, T);
	fflush(libLogFile);
	
	//define how much memory to use on device
	usableMem = (((gpuProp.totalGlobalMem * 75) / 100) / sizeof(glob)) * sizeof(glob);//75% of total device memory
	//define computational bounds
	kInfo_host = (storage *)malloc(structByteSize);
	kInfo_host->kStart = 0;//TODO:Implement algorithm to calculate smallest possible valid kValue
	kInfo_host->kRoot = 0;
	kInfo_host->nodes = nodes;
	kInfo_host->edges = edges;
	kInfo_host->kBitSize = ((kInfo_host->nodes * kInfo_host->nodes) - kInfo_host->nodes) / 2;//bit size of Kvalue
	kInfo_host->bitLength = bitLength;
	kInfo_host->kEnd = ktype(pow(double(2), double(kInfo_host->kBitSize))-1);//change this to solve for the largest k value that can't be a value

	//allocate memory on host & device
	host_glob = (glob *)malloc(usableMem);
	logCudaError(cudaMalloc((void **)&device_glob, usableMem),"Allocate glob");//allocate storage array on device
	host_storage = (ktype *)malloc(1);//included to prevent debugging errors in VS.
	logCudaError(cudaMalloc((void **)&device_storage, structByteSize), "Allocate kInfo");//allocate array to store kInfo; must use structByteSize as sizeOf(storage) may return size including padding
	logCudaError(cudaMalloc((void **)&device_flag, sizeof(char)),"Allocate device flag");//allocate flag on device
	logCudaError(cudaMemcpy(device_flag, &host_flag, sizeof(char), cudaMemcpyHostToDevice),"Set Device Flag to 0");//set device value to 0

	//Iterate through all possible solutions of the kValue.
	globMemUsed = 0;
	count = kInfo_host->kRoot;
	update = fopen(updatePath, "w");
	fprintf(update, "%d", count);
	fclose(update);
	for (i = kInfo_host->kRoot; i < kInfo_host->kEnd; i += (B*T), globMemUsed += (B*T), count+=(B*T)) { //Increment by the size of the CUDA grid
		if (!(count%giga)) {
			update = fopen(updatePath, "w");
			fprintf(update, "%d", count);
			fclose(update);
			
		}
		if (globMemUsed + (B*T)>usableMem && globMemUsed != 0) {//check to ensure you won't overflow glob and lose data
			logCudaError(cudaMemcpy(&host_flag, device_flag, sizeof(char), cudaMemcpyDeviceToHost),"Copy flag from device to host");
			if (host_flag) {
				//write glob to host
				logCudaError(cudaMemcpy(host_glob, device_glob, usableMem, cudaMemcpyDeviceToHost), "Copy glob to host from device; kEnd not reached");
				//strip glob to file
				sprintf(filePath, "%s\\results\\%d_%d.bill", rootPath, nodes, edges);
				solutionCount += stripGlobToFile(host_glob, kInfo_host->kRoot, i - 1, filePath);
			}
			//reset values
			kInfo_host->kRoot = i;//reset the Root kValue of the new file
			globMemUsed = 0;
			host_flag = 0;
			logCudaError(cudaMemcpy(device_flag, &host_flag, sizeof(char), cudaMemcpyHostToDevice), "Reset Device Flag to 0");//Reset device flag to 0
		}
		kInfo_host->kStart = i;//defines the starting point of the new kernel call.
		squashStruct(kInfo_host, &host_storage);//squash to prep for transfer to device
		logCudaError(cudaMemcpy(device_storage, host_storage, structByteSize, cudaMemcpyHostToDevice), "update kInfo");//transfer to device
		/*
		KERNEL START
			Currently can only utilize 1 GPU
		*/
		kValid <<<B, T>>> (device_storage, device_glob, device_flag);//execute the kernel
		logCudaError(cudaDeviceSynchronize(), "Synchronize Kernels");//wait for device to finish before continuing b/c it's single threaded
		/*
		KERNEL END
		*/
		fflush(libLogFile);
	}//END FOR LOOP
	update = fopen(updatePath, "w");
	fprintf(update, "%d", count);
	fclose(update);
	logCudaError(cudaMemcpy(&host_flag, device_flag, sizeof(char), cudaMemcpyDeviceToHost), "Copy flag from device to host");
	if (host_flag) {
		//retrieve final computational run; this run's last value is kFinal-1
		logCudaError(cudaMemcpy(host_glob, device_glob, usableMem, cudaMemcpyDeviceToHost), "Copy glob to host from device; kEnd reached");
		//strip glob to file
		sprintf(filePath, "%s\\results\\%d_%d.bill", rootPath, nodes, edges);
		solutionCount += stripGlobToFile(host_glob, kInfo_host->kRoot, kInfo_host->kEnd - 1, filePath);
	}
	
	//deallocate Device memory
	logCudaError(cudaFree(device_glob), "Free device_glob");
	//deallocate Host memory
	free(kInfo_host);
	free(host_glob);
	free(host_storage);

	return solutionCount;
 }

void buildFileTree(char * rootPath) {
	char newPath[500];

#ifdef _WIN64
	if (!PathFileExists(rootPath)) {
		if (!CreateDirectory(rootPath, NULL)) {
			fprintf(libLogFile, "ERROR: Unable to create root directory. Exiting program...");
			exit(-2);
		}
	}
	sprintf(newPath, "%s\\results", rootPath);//define results folder
	if (!PathFileExists(newPath)) {
		if (!CreateDirectory(newPath, NULL)) {
			fprintf(libLogFile, "ERROR: Unable to create results directory. Exiting program...");
			exit(-3);
		}
	}
	sprintf(newPath, "%s\\temp_results", rootPath);//define temp_results folder
	if (!PathFileExists(newPath)) {
		if (!CreateDirectory(newPath, NULL)) {
			fprintf(libLogFile, "ERROR: Unable to create temp_results directory. Exiting program...");
			exit(-4);
		}
	}
	sprintf(newPath, "%s\\logs", rootPath);//define logs folder
	if (!PathFileExists(newPath)) {
		if (!CreateDirectory(newPath, NULL)) {
			fprintf(libLogFile, "ERROR: Unable to create logs directory. Exiting program...");
			exit(-5);
		}
	}
//#elif __linux__
#endif
}

void logCudaError(cudaError_t error, const char * userInfo) {
	if (error != cudaSuccess) {
		fprintf(cudaLogFile, "CUDA ERROR: %s : %s >> %s\n", cudaGetErrorName(error), cudaGetErrorString(error), userInfo);
		fflush(cudaLogFile);
	}
}

static void squashStruct(storage * source, ktype **dest) {
	*dest = (ktype *)calloc(structSize, sizeof(ktype));
	(*dest)[0] = source->kStart;
	(*dest)[1] = source->kEnd;
	(*dest)[2] = source->kRoot;
	(*dest)[3] = source->nodes;
	(*dest)[4] = source->edges;
	(*dest)[5] = source->kBitSize;
	(*dest)[6] = source->bitLength;
}

static void dumpGlobToBinFile(glob *source, ktype input_count, ktype start, ktype end, char *filePath) {
	//create temp file
	FILE *dest = fopen(filePath, "wb");
	ktype buffer[3] = { start,end,input_count };

	//metadata
	fwrite(&buffer,sizeof(ktype),3,dest);
	//glob
	fwrite(source, sizeof(glob), input_count, dest);
	//flush buffer
	fclose(dest);
}

void convertFiles(char *rootPath, int node) {
	HANDLE folderHandle, fileHandle;
	WIN32_FIND_DATA fileData, folderData; //used to store information about Windows files & directories

	FILE * resultFile, *outputFile;
	char *storagePath, *tempString, *filePath;//tempString stores the search parameters for FindFirst(Next)File fucntions
	ktype start, end, globSize;
	glob * results;


	tempString = (char *)malloc(500);
	storagePath = (char *)malloc(500);
	filePath = (char *)malloc(500);

	//find the first {node}_{edge} folder
	sprintf(tempString, "%s\\temp_results\\%d_*", rootPath, node);
	folderHandle = FindFirstFile(tempString, &folderData);//folderData is passed to this function and then recieves data; folderHandle is assigned the Handle pointer returned by the function
	if(folderHandle != INVALID_HANDLE_VALUE){
		do {
			sprintf(storagePath, "%s\\temp_results\\%s", rootPath, folderData.cFileName);//get the folder in question
			//find the first file and store handle
			sprintf(tempString, "%s\\*.bin", storagePath);
			fileHandle = FindFirstFile(tempString, &fileData);//fileData is passed to this function and then recieves data; fileHandle is assigned the Handle pointer returned by the function 
			if (fileHandle != INVALID_HANDLE_VALUE) {
				do {
					sprintf(filePath, "%s\\%s", storagePath, fileData.cFileName);
					//open temp_results file for reading
					resultFile = fopen(filePath, "rb");
					//create bill's output file for writing
					sprintf(tempString, "%s\\results\\%s.bill", rootPath, folderData.cFileName);
					outputFile = fopen(tempString, "w");
					//read in File
					fread(&start, sizeof(ktype), 1, resultFile);
					fread(&end, sizeof(ktype), 1, resultFile);
					fread(&globSize, sizeof(ktype), 1, resultFile);
					results = (glob *)malloc(globSize);
					fread(results, sizeof(glob), globSize, resultFile);
					for (ktype i = 0; i <= (end - start); i++) {
						if (results[i]) { //if a solution is at the current location
							fprintf(outputFile, "%llu\n", (start + i));
						}
					}
					//close read file
					fclose(resultFile);
				} while (FindNextFile(fileHandle, &fileData));
				//close output file
				fclose(outputFile);
			}
			//remove the binary data
			sprintf(tempString, "%s\\*", storagePath);
			fileHandle = FindFirstFile(tempString, &fileData);
			if (fileHandle != INVALID_HANDLE_VALUE) {
				do {
					sprintf(tempString, "%s\\%s", storagePath, fileData.cFileName);
					remove(tempString);
				} while (FindNextFile(fileHandle, &fileData));
			}
			RemoveDirectory(storagePath);
		} while (FindNextFile(folderHandle, &folderData));
	}

	//deallocate memory
	free(tempString);
	free(storagePath);
	free(resultFile);
	//free(&fileData);
	//free(&folderData);
	free(results);
}

static ktype stripGlobToFile(glob * source, ktype start, ktype end, char * filePath) {
	FILE * output = fopen(filePath, "a");
	ktype i, count=0;

	//loop through glob
	for (i = 0; i <(end - start + 1); i++) {
		if (source[i]) {
			fprintf(output, "%llu\n", (start + i));
			count++;
		}
	}
	fflush(output);
	fclose(output);
	//free(output);

	return count;
}

void initializeLogFiles(char *rootPath) {
	char filePath[500];

#ifdef _WIN64
	//CUDA Log
	sprintf(filePath,"%s//logs//CUDA.log",rootPath);
	cudaLogFile = fopen(filePath,"w");//overwrites file if already there
	//Device Log
	sprintf(filePath, "%s//logs//host.log", rootPath);
	libLogFile = fopen(filePath, "w");//overwrites file if already there
//#elif __linux__
#endif
}

void closeLogFiles() {
	fclose(cudaLogFile);
	fclose(libLogFile);
}