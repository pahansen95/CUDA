/*
This file is peter's implementation of his Graph CUDA library.
*/

//OS Defined headers
#include<direct.h>
//self defined headers
#include "graph_cuda.cuh"
//C Headers
//CUDA C Headers

int main() {
	const char * rootPath = _getcwd(NULL,500); //defines the abosolute path the file system will be created at. This can never change.
	char tempPath[500];//used for generating a path string
	int nodeStart = 9, nodeEnd = 10, node, edge;
	unsigned long long int solutionCount;

	//setup environment
	sprintf(tempPath, "%s", rootPath);
	buildFileTree(tempPath);
	initializeLogFiles(tempPath);

	//printf("%d\n", sizeof(unsigned int) * 8);
	//printf("%d\n", sizeof(unsigned long int) * 8);
	//printf("%d\n", sizeof(unsigned long long int) * 8);


	//Loop through range of nodes desired
	for (node = nodeStart; node <= nodeEnd; node++) {
		printf("Starting Computations for %d NODES", node);
		for (edge = 2; edge < node; edge++) {//0 & node edges will always have 0 solutions
			if ((node * edge) % 2) {//skip graphs which have no solutions
				continue;
			}
			if (edge == 1) {
				printf("\n\t%d EDGE...", edge);
			}
			else {
				printf("\n\t%d EDGES...", edge);
			}
			solutionCount = solveGraph(node, edge, tempPath);
			printf("\t%llu Valid Solutions", solutionCount);
		}//END INNER FOR LOOP
		printf("\n");
		//convert all node solutions to human readable form
		//printf("\nGenerating human readable files for all solutions from this node family...\n");
		//convertFiles(tempPath, node);
	}//END OUTER FOR LOOP
	
	//close log files
	closeLogFiles();

	printf("Press Enter to exit");
	getchar();
	return 0;
}