/*
This header file defines functions, structs and variables that should only be used by the graph_cuda implementation.
IT SHOULD NEVER BE INCLUDED BY OTHERS.

graph_cuda includes all the non-kernel functions necessary to conduct GPU accelerated computations of a graph of X nodes and Y edges

DEFINITIONS:
glob: a large collection of data in any generic form.
*/

//C Headers
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//CUDA C Headers
#include "device_launch_parameters.h"
#include "cuda.h"
#include "cuda_runtime.h"

/*
These Typedefs are not meant for use outside of the corresponding ".cu" file
*/
typedef unsigned long long int ktype; //The largest basic data type that can hold the bits of a kValue.
typedef unsigned char glob; //The data type of the large array on the GPU device which stores results. See definition at top for more info.

#define B	256 //Blocks in a CUDA grid. Subject to change.
#define T	256 //Threads in a CUDA block. Subject to change.

const int bitLength = sizeof(glob) * 8; //denotes the length in bits of the data type of the kValues on the device.
FILE *cudaLogFile, *resultFile, *libLogFile;
const int structSize = 7;
const size_t structByteSize = sizeof(ktype) * structSize;

struct storage {
	ktype kStart; //define the relative kvalue that the kernel starts on; ie the current i value in a for loop;
	ktype kEnd; //defines the absolute kValue at which point no other kValues at or above it can be a possible graph.
	ktype kRoot; //defines the absolute kValue of the first value in the device data array, this value changes when you offload the array from the device
	ktype nodes; //how many nodes
	ktype edges; //how many edges
	ktype kBitSize; //how large the kValue is in bits
	ktype bitLength; //denotes the length in bits of the data type of the kValues on the device. Used in Kernel.
};

/*
squashStruct converts a struct into a 1-dimensional array of a static size so that you may pass struct
information to a CUDA Kernel. It takes in a pointer to the source storage struct and
a pointer to an unallocated dynamic array (pointer to a pointer) which will hold the
the contents.
*/
static void squashStruct(storage * source, ktype ** dest);

/*
writeToFile writes metadata & a glob retrieved from the GPU device into a binary file for temporary storage.
*/
static void dumpGlobToBinFile(glob * source, ktype count, ktype start, ktype end, char * filePath);

/*
stripGlobToFile takes a fresh glob and iterates through it finding all solutions and then appending them to a file defined by filePath.
It returns the # of solutions added to the file.
*/
static ktype stripGlobToFile(glob * source, ktype start, ktype end, char * filePath);

