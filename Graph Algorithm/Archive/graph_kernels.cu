/*
This file defines all kernels that may be publically used.
*/

//Self Defined Headers
#include "graph_kernels.cuh"

__global__ void kValid(ktype *kInfo, glob *values, char * flag) {
	//to calculate the current position in the Device Array use the following formula: (blockId*threadId) + threadId + kStart -(absolute kValue of position 0)
	ktype rowCount, kBitLocation, kMirrorBitLocation, row, col, kStart,kEnd, kRoot, nodes, edges, kSize, bitLength, curK;

	kStart = kInfo[0];
	kEnd = kInfo[1];
	kRoot = kInfo[2];
	nodes = kInfo[3];
	edges = kInfo[4];
	kSize = kInfo[5];
	bitLength = kInfo[6];

	curK = blockIdx.x*blockDim.x + threadIdx.x + kStart;
	if (curK >= kEnd) {//check to make sure you don't overshoot the end value
		return;
	}

	kBitLocation = 1;//assuming the first bit in the kvalue has a position 1;
	for (row = 0; row < nodes; row++) {
		rowCount = 0;
		kMirrorBitLocation = row;//the bit position for the mirrored kvals is always starts at the row value (assuming the first row has a position of 0)
		for (col = 0; col < nodes; col++) {
			if (col > row) {
				if (curK & (ktype(1) << (kSize - kBitLocation))) {//add one to kIterator to convert to counting space
					rowCount++;
				}
				kBitLocation++;
			}
			if (col < row) {
				if (col > 0) {
					kMirrorBitLocation += (nodes - 2) - (col - 1);
				}
				if (curK & (ktype(1) << (kSize - kMirrorBitLocation))) {//if bit is set
					rowCount++;
				}
			}
		}
		if (rowCount != edges) {
			//set the ith bit to zero
			values[curK - kRoot] = 0;
			return;
		}
	}
	//set the ith bit to one
	values[curK - kRoot] = 1;
	*flag = 1;//not a race condition
}