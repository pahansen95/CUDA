/*
This Header file defines functions, structs & variables that may be called by 3rd party library users inorder to conduct GPU accelerated
computations on a graph of X nodes and Y edges.

*/

//C Headers
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//CUDA C Headers
#include "device_launch_parameters.h"
#include "cuda.h"
#include "cuda_runtime.h"

const unsigned long long int kilo = 1000, mega = 1000000, giga = 1000000000, tera = 1000000000000;

/*
Report Cuda Error is a helper function which takes a single "cudaError_t" and creates  
a character array which contains the error's name and it's description. this character 
is the logged. This function should wrap all CUDA API function calls that return a 
cudaError_t. This function SHOULD NOT wrap a CUDA kernel as they must return void.
*/
void logCudaError(cudaError_t error, const char * userInfo);

/*
solveGraph takes a graph of X nodes & Y edges and produces all of the possible solutions.
It returns the total number of valid graphs found.
It takes in the Nodes, edges and then an absolute path to the storage location.
At this time only a single GPU can be utilizied
*/
unsigned long long int solveGraph(int nodes, int edges, char * rootPath);

/*
buildFileTree does the initial building of the directory framework for use by the CUDA portion of the program.
It takes the root directory as defined by the user.
The File Tree is as follows
...
Root Directory/
	|-> results/
	|	|-> {node}_{edge}.bill
	|-> temp_results/
	|	|-> {node}_{edge}/
	|		|-> {kVal}.bin
	|-> logs/
		|-> CUDA.log
		|-> host.log
*/
void buildFileTree(char * rootPath);

/*
initializeLogFiles is called to create Logfiles. This function should be preceded by buildFileTree.
*/
void initializeLogFiles(char *rootPath);

/*
closeLogFiles closes all log files for writing. Calling initializeLogFiles will wipe them.
*/
void closeLogFiles();

/*
convertFiles takes all binary files of a glob and converts it into a human readable form. Each line has the decimal
representation of a valid kValue.
*/
void convertFiles(char *rootPath, int node);



