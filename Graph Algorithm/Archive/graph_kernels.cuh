/*
This Header file prototypes ONLY CUDA Kernels
It is discouraged to directly use these functions as they have already been implemented in wrapper functions.
*/

//CUDA C Headers
#include "device_launch_parameters.h"
#include "cuda.h"
#include "cuda_runtime.h"
#include "device_functions.h"

typedef unsigned long long int ktype;
typedef unsigned char glob;

/*
kValid loops through the matrix representation of the graph and determines if the current kValue is a valid solution
or not.
It takes in a storage struct squashed into an array, a device array where each position represents if a kValue is
valid or not, & a char used as a flag to determine if a chunk has a solution in it.
*/
__global__ void kValid(ktype * kInfo, glob *values, char * flag);