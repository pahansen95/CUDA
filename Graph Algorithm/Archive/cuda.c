typedef unsigned long int ktype;
typedef unsigned int uint;
typedef unsigned short int encode;

struct storage {
	ktype start;
	kytpe end;
	uint nodes;
	uint edges;
	uint num_threads;
	encode *d_result;
}

encode* kernel_execute(ktype start, ktype end, uint nodes, uint edges){
	//980ti supports compute capability 5.2, warp size is 32, optimized block # will be a multiple of  and somewhere between 128 & 512 blocks
	//980ti has 2816 Cuda Cores
	//For choosing Thread and block number reference  see: https://stackoverflow.com/questions/9985912/how-do-i-choose-grid-and-block-dimensions-for-cuda-kernels
	//read in a start and end value for k as well as the # of nodes and edges
	struct storage cuda_param;
	cudaDeviceProp * devProps;
	uint num_blocks, num_threads;
	int i;
	ktype kval_chunk, k;
	encode *result;
	encode *d_result;
	ktype size = (end - start +1)/8 + 1; //generate array of 8 bit ints where each bit represents whether a function is valid or not. add 1 to ensure enough space to store all information
	cudaGetDeviceProperties(devProps, 0); 	//get device information
	//determine the # of blocks
	//devProps->
	num_blocks = 4; //for now assume block size will be 4
	//determine # of threads/block
	num_threads = 256; //for now assume 256 threads
	//total possible evaluated kvalues will be blocks*threads (4*256)
	kval_chunk = (start-end+1)/(num_blocks*num_threads);
	
	//TODO generate an array on the device to store results of kValid
	
	cudaMalloc((void **)&d_result, size);
	*result = (encode *)malloc(size);
	
	cuda_param.start = start;
	cuda_param.end = end;
	cuda_param.nodes = nodes;
	cuda_param.edges = edges;
	cuda_param.num_threads = num_threads;
	cuda_param.d_result = d_result;

	i = 0;
	for(k=start; k<=end;k+=kval_chunk){
		if(i == 8){
			
			i = 0
		}
		if(k+kval_chunk > end){//I'm tired, I'm not 100% on my logic here
			cuda_param.start = k;
			cuda_param.end = end;
			<<int(num_blocks),int((start-end+1)%(num_blocks*num_threads))>>kernel(cuda_param);
			
		} else {
			cuda_param.start = k;
			cuda_param.end = k+kval_chunk;
			<<int(num_blocks),int(num_threads)>>kernel(cuda_param);
		}
		i++;
	}
	
	cudaMemcpy(result, d_result, size, cudaMemcpyDeviceToHost);
	
	return result;
}

__global__ encode* kernel(storage *cuda_param){
	ktype offset =  ktype(threadIdx.x + blockIdx.x * storage->num_threads);
	kValid(start+offset)
}

__device__ int kValid(ktype& kVal, int& kSize){
	ktype rowCount, kIterator, kMirrorIterator, row, col;
	kIterator = 0;
	for (row = 0; row < nodes; row++){
		rowCount = 0;
		kMirrorIterator = row-1;//convert from counting to iterator space
		for (col =0; col < nodes; col++){
			if (col > row){ 
				if((kVal>>(kSize-1-kIterator))&1 == 1){//gets bit at nth iterator location where the 0th position is the farthest left most significant bit (ie kSize)
					rowCount++;
				}
				kIterator++;
			}
			if (col < row){
				if (col > 0){
					kMirrorIterator += nodes - (col + 1);
				}
				if ((kVal>>(kSize-1-kMirrorIterator))&1 == 1){//if bit is set
					rowCount++;
				}
			}
			if (row == col){ //diagonal is always 0. Not Necessary but included for clarity's sake
				continue;
			}
		}
		if (rowCount != edges){
			return 0;
		}
	}
	return 1;
}