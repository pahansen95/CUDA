//
//  main.c
//  Graph_Algorithm
//
//  Created by Bill Forsyth on 8/30/17.
//  Copyright © 2017 Bill Forsyth. All rights reserved.
//

//If calculations are correct, max supported nodes is 31 (ktype runs out of bits)

#include <stdio.h>
#include <math.h>
#include <inttypes.h>
#include <stdlib.h>

typedef unsigned long int ktype;
typedef unsigned int uint;
typedef unsigned short int encode;

uint nodes = 0;
uint edges = 0; 

//int kValid(ktype, int);
void printMatrix(ktype, int);
void kValue_to_python_file(ktype, int);

int main(int argc, const char * argv[]) {
    nodes = atoi(argv[1]);
    edges = atoi(argv[2]);
    ktype i;
    ktype start = (ktype) atoll(argv[3]);
    ktype end = (ktype) atoll(argv[4]);
    int thread_number = atoi(argv[5]);
    
    ktype numValid = 0;
    int kSize = ((nodes * nodes)-nodes)/2;
    FILE * fp;
    char filename[128];
    sprintf(filename, "bashgraph/log/Thread-%i-log.txt", thread_number);
	fp = fopen(filename, "w+");
    
    fprintf(fp, "%i-%i\n", nodes, edges);
    fprintf(fp, "K magnitude: %i\n", kSize);
    //fprintf(fp, "NumGraphs: %llu\n", numGraphs);
    fprintf(fp, "Start: %llu\n", start);
    fprintf(fp, "End: %llu\n", end);
	
	
	
    /* for(i = start; i<end; i++){
        if(kValid(i,kSize)==1){
            numValid++;
			kValue_to_python_file(i, kSize);
            fprintf(fp, "K-value %llu, ", i);
            for(int x = 0; x<kSize; x++){
                fprintf(fp, "%u", (i>>(kSize-1-x))&1);//ToDo Print out Binary Value of K, I'm not sure If I'm doing this correctly
            }
            fprintf(fp, " - Valid\n");
        }
    } */
    
    fprintf(fp, "\nThere were %llu valid graphs\n", numValid);
    fclose(fp);
    return 0;
}



//Kvalid has been moved into a cuda kernel
/*//kValid uses only the Kvalue to check if a graph is valid. kSize is the total # of possible graphs (both valid & invalid)
int kValid (ktype kVal, int kSize){
	ktype rowCount, kIterator, kMirrorIterator, row, col;
	kIterator = 0;
	for (row = 0; row < nodes; row++){
		rowCount = 0;
		kMirrorIterator = row-1;//convert from counting to iterator space
		for (col =0; col < nodes; col++){
			if (col > row){ 
				if((kVal>>(kSize-1-kIterator))&1 == 1){//gets bit at nth iterator location where the 0th posistion is the farthest left most significant bit (ie kSize)
					rowCount++;
				}
				kIterator++;
			}
			if (col < row){
				if (col > 0){
					kMirrorIterator += nodes - (col + 1);
				}
				if ((kVal>>(kSize-1-kMirrorIterator))&1 == 1){//if bit is set
					rowCount++;
				}
			}
			if (row == col){ //diagonal is always 0. Not Necessary but inlcuded for clarity's sake
				continue;
			}
		}
		if (rowCount != edges){
			return 0;
		}
	}
	return 1;
}*/

void printMatrix(ktype kVal, int kSize){
	int row, col, rowCount, kIterator, kMirrorIterator;
	kIterator = 0;
	for(row = 0; row<nodes; row++){
		rowCount = 0;
		kMirrorIterator = row-1;//convert from counting to iterator space
		for(col = 0; col<nodes; col++){
			if (col > row){
				if ((kVal>>(kSize-1-kIterator)) & 1){//if bit is set
					printf("%d", 1);
				} else {
					printf("%d", 0);
				}
				kIterator++;
			}
			if (col < row){
				if (col > 0){
					kMirrorIterator += (nodes - 1) - col;
				}
				if ((kVal>>(kSize-1-kMirrorIterator)) & 1){//if bit is set
					printf("%d", 1);
				} else {
					printf("%d", 0);
				}
			}
			if (row == col){ //diagonal is always 0
				printf("%d", 0);
			}
		}
	}
}

void kValue_to_python_file(ktype kVal, int kSize){
	ktype kIterator, kMirrorIterator;
	int row, col, rowCount;
    FILE * fp;
    char filename[128];
    sprintf(filename, "bashgraph/graphs/%i-%i-K%llu.txt", nodes, edges, kVal);
	fp = fopen(filename, "w+");
    
	kIterator = 0;
    for(row = 0; row<nodes; row++){
		rowCount = 0;
		kMirrorIterator = row-1;//convert from counting to iterator space
		fprintf(fp, "ID:%i\n", row);
        for(col = 0; col<nodes; col++){
			if (col > row){ 
				if((kVal>>(kSize-1-kIterator))&1 == 1){//gets bit at nth iterator location where the 0th posistion is the farthest left most significant bit (ie kSize)
					fprintf(fp, "\tN:%i\n", col);
				}
				kIterator++;
			}
			if (col < row){
				if (col > 0){
					kMirrorIterator += nodes - (col + 1);
				}
				if ((kVal>>(kSize-1-kMirrorIterator))&1 == 1){//if bit is set
					fprintf(fp, "\tN:%i\n", col);
				}
			}			
        }
    }
  
    fclose(fp);
}
