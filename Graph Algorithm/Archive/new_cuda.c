#include <stdio.h>
#include <stdlib.h> 
#include <cstring>
#include <math.h>

#include "device_launch_parameters.h"
#include "cuda.h"
#include "cuda_runtime.h"
#include "helper_cuda.h"

typedef unsigned long long int ktype;

#define B	256
#define T	256

const int bitLength = sizeof(ktype) * 8; //denotes the length in bits of the data type of the kValues on the device.
FILE *logFile, *dataFile;
const int structSize = 6;
const size_t structByteSize = sizeof(ktype) * structSize;

struct storage {
	ktype kStart; //define the relative kvalue that the kernel starts on; ie the current i value in a for loop;
	ktype kRoot; //defines the absolute kValue of the first value in the device data array, this value changes when you offload the array from the device
	ktype nodes; //how many nodes
	ktype edges; //how many edges
	ktype kSize; //how large the kValue is in bits
	ktype bitLength; //denotes the length in bits of the data type of the kValues on the device. Is this redundent? 
};

void writeToLog(char * c) {
	fputs(c, logFile);
	fputs("\n", logFile);
}

void reportCudaError(cudaError_t error) {
	const char * temp;
	char * copy;

	if (error != cudaSuccess) {
		temp = cudaGetErrorName(error);
		copy = (char *)malloc(strlen(temp) + 1); //see https://stackoverflow.com/questions/481673/make-a-copy-of-a-char for making a copy of a const char *
		strcpy(copy, temp);
		writeToLog(copy);
		temp = cudaGetErrorString(error);
		copy = (char *)malloc(strlen(temp) + 1); //see https://stackoverflow.com/questions/481673/make-a-copy-of-a-char for making a copy of a const char *
		strcpy(copy, temp);
		writeToLog(copy);
		free(copy); //free memory
	}
	else {
		writeToLog("No CUDA Error");
	}
}

__global__ void kValid(ktype *kInfo, ktype *values) {
	//to calculate the current position in the Device Array use the following formula: (blockId*threadId) + threadId + kStart -(absolute kValue of position 0)
	ktype rowCount, kIterator, kMirrorIterator, row, col, kStart, kRoot, nodes, edges, kSize, bitLength;

	kStart = kInfo[0];
	kRoot = kInfo[1];
	nodes = kInfo[2];
	edges = kInfo[3];
	kSize = kInfo[4];
	bitLength = kInfo[5];

	kIterator = 0;
	for (row = 0; row < nodes; row++) {
		rowCount = 0;
		kMirrorIterator = row - 1;//convert from counting to iterator space
		for (col = 0; col < nodes; col++) {
			if (col > row) {
				if (((kStart + ktype(blockIdx.x*blockDim.x + threadIdx.x)) >> (kSize - 1 - kIterator)) & 1 == 1) {//gets bit at nth iterator location where the 0th position is the farthest left most significant bit (ie kSize)
					rowCount++;
				}
				kIterator++;
			}
			if (col < row) {
				if (col > 0) {
					kMirrorIterator += nodes - (col + 1);
				}
				if (((kStart + ktype(blockIdx.x*blockDim.x + threadIdx.x)) >> (kSize - 1 - kMirrorIterator)) & 1 == 1) {//if bit is set
					rowCount++;
				}
			}
			if (row == col) { //diagonal is always 0. Not Necessary but included for clarity's sake
				continue;
			}
		}
		if (rowCount != edges) {
			//return 0;
			//set the ith bit; see here https://www.quora.com/How-do-you-set-clear-and-toggle-a-single-bit-in-C
			values[((blockIdx.x*blockDim.x + threadIdx.x) + kStart - kRoot) / bitLength] ^= (-0 ^ values[((blockIdx.x*blockDim.x + threadIdx.x) + kStart - kRoot) / bitLength]) & (1 >> values[((blockIdx.x*blockDim.x + threadIdx.x) + kStart - kRoot) % bitLength]);//should I left shift instead?
			//(ktype(blockIdx.x*threadIdx.x + threadIdx.x) + kStart - kRoot) : the current kValue's bit location;
			//(ktype(blockIdx.x*threadIdx.x + threadIdx.x) + kStart - kRoot) / bitlength : the current kValue's bit's "block" location where a block is the variable in the array that holds the bit.
			return;
		}
	}
	//set the ith bit; see here https://www.quora.com/How-do-you-set-clear-and-toggle-a-single-bit-in-C
	values[((blockIdx.x*blockDim.x + threadIdx.x) + kStart - kRoot) / bitLength] ^= (-1 ^ values[((blockIdx.x*blockDim.x + threadIdx.x) + kStart - kRoot) / bitLength]) & (1 >> values[((blockIdx.x*blockDim.x + threadIdx.x) + kStart - kRoot) % bitLength]);//should I left shift instead?
	//see explanation above
}

//structToArray sqaushes a struct into an array to allow passing of information to kernel. Takes a pointer to an already populated struct and a pointer to a pointer of an array which it will initialize. It is up to the programmer to deallocate memory in their calling function.
void structToArray(storage * values, ktype **array) {
	*array = (ktype *)calloc(structSize,sizeof(ktype));
	(*array)[0] = values->kStart;
	(*array)[1] = values->kRoot;
	(*array)[2] = values->nodes;
	(*array)[3] = values->edges;
	(*array)[4] = values->kSize;
	(*array)[5] = values->bitLength;
}

//WriteToFile is temporarily used to write computations to a simple binary file. The file is such: start, end, # of kvals then glob of computations. start & end are unsigned long ints while the array is of unsigned ints
void writeToFile(ktype * input, size_t input_bytesize, ktype start, ktype end) {
	//ktype * input_copy;//Creating a copy of the inputs for the purpose of multithreading, not necessary if single threaded.
	//ktype buffer = input_bytesize;//defines the length in bytes of the data glob to be written; used for specifying how many bytes to read later. 
	
	//buffer = size_t(0);
	//input_copy = (ktype *)malloc(input_bytesize);//allocate memory; 
	//memcpy(&input_copy, &input, input_bytesize);
	//buffer = input_bytesize;
	//writeToLog("Attempt File");
	//FILE *fp = fopen("data.dat", "ab");//Create or Open binary File
	//writeToLog("Opened file");
	fwrite(&start, sizeof(ktype), 1, dataFile);
	fwrite(&end, sizeof(ktype), 1, dataFile);
	fwrite(&ktype(input_bytesize), sizeof(ktype), 1, dataFile);
	fwrite(input, sizeof(ktype), input_bytesize / sizeof(ktype), dataFile);
	//fclose(dataFile);//close file 
	//free(input_copy);//deallocate memory
}

//convertFile Converts the kValue output into the Type required by Bill©. WARNING: Only call this function after you have closed the output file.
void convertFile() {
	ktype start, end, bufferSize;
	ktype * glob;
	char * tempString;
	FILE *billFile = fopen("output.bill", "w");
	FILE *input = fopen("data.dat", "rb");

	fread(&start, sizeof(ktype), 1, input);
	fread(&end, sizeof(ktype), 1, input);
	fread(&bufferSize, sizeof(ktype), 1, input);
	printf("%llu", start);
	printf("\n");
	printf("%llu", end);
	printf("\n");
	printf("%llu", bufferSize);
	printf("\n");

	glob = (ktype *)malloc(bufferSize);
	tempString = (char *)malloc(sizeof(ktype));

	fread(glob, sizeof(ktype), bufferSize /sizeof(ktype), input);
	printf("%s","Converting File");
	for (ktype i = 0; i < end; i++) {
		if ((glob[i/sizeof(ktype)]>>(sizeof(ktype)-(i%sizeof(ktype)))) & 1 == 1) {//check if bit is set
			printf("%llu", (start + i));
			printf("\n");
			sprintf(tempString, "%llu", (start+i));
			fwrite(tempString, sizeof(ktype), 1, billFile);
			fwrite("\n", sizeof(char), 1, billFile);
		}	
	}
	//close files
	fclose(input);
	fclose(billFile);
	//free memory
	free(glob);
	free(tempString);
}

int main(int argc, const char *argv[]) {
	cudaDeviceProp prop;
	ktype *device_values, *host_values;
	struct storage * kInfo_host = (storage *) malloc(structByteSize);
	ktype *param_host, *param_device;
	ktype start, end;
	ktype usableMem, bitsUsed;
	int cudaCount = -1;
	char * tempString = (char *) malloc(sizeof(int));
	logFile = fopen("CUDA.log", "w");//initialize log file
	dataFile = fopen("data.dat", "wb");
	param_host = (ktype *)malloc(1);//initialize to prevent debug errors
	
	//DEBUGGING PURPOSES
	//size_t temp = sizeof(usableMem);
	//END DEBUGGING PURPOSES

	writeToLog("Program Start");

	reportCudaError(cudaSetDevice(0));
	reportCudaError(cudaGetDeviceProperties(&prop, 0));
	//reportCudaError(cudaGetDeviceCount(&cudaCount));
	writeToLog(itoa(cudaCount, tempString, 10));
	writeToLog(prop.name);
	//usableMem = (prop.totalGlobalMem * 9) / 10 ; //define how much device Memory is to be used; for use with large RAM sizes
	usableMem = 100000000; //100 MegaBytes; for use on development on lightweight systems
	host_values = (ktype *)malloc(usableMem); //dynamically allocate host array to same size as device array
	//start = atoi(argv[3]);
	start = 0; //TODO: implement solution
	//end = atoi(argv[4]);
	kInfo_host->kStart = start;
	kInfo_host->kRoot = start;
	//kInfo_host->nodes = atoi(argv[1]);;//TODO
	//kInfo_host->edges = atoi(argv[2]);;//TODO
	kInfo_host->nodes = 5;
	//end = 1024;
	kInfo_host->edges = 2;
	kInfo_host->kSize = ((kInfo_host->nodes * kInfo_host->nodes) - kInfo_host->nodes) / 2;//TODO
	kInfo_host->bitLength = bitLength;
	end = ktype(pow(double(2), double(kInfo_host->kSize)));
	//structToArray(kInfo_host, param_host);
	//param_host = ;//allocate an array the same size as the storage struct to squash for passing to kernel 

	//HANDLE_ERROR( cudaMalloc((void**)&data.dev_bitmap,bitmap.image_size() ) )
	reportCudaError(cudaMalloc((void**)&device_values, usableMem));//allocate storage array on device
	reportCudaError(cudaMalloc((void**)&param_device, structByteSize));//allocate array to store kInfo; must use structByteSize as sizeOf(storage) may return szie including padding
	bitsUsed = 0;
	for (ktype i = start; i < end; i += (B*T)) { //iterate through kVals incrementing by the # of kernel Blocks * kernel threads
		if (bitsUsed + (B*T)>usableMem * 8) {//check for potential array overflow. If so then copy off data and reset device array
			writeToLog("Array Full");
			reportCudaError(cudaMemcpy(host_values, device_values, usableMem, cudaMemcpyDeviceToHost));//retrieve computations
			writeToFile(host_values, usableMem, kInfo_host->kRoot, i); //dump results into file
			kInfo_host->kRoot = i;//reset Root value when you reset the array
			bitsUsed = 0;//reset bit count
			reportCudaError(cudaFree(device_values));//is there a better way of doing this, like just reusing the array
			reportCudaError(cudaMalloc((void**)&device_values, usableMem));//allocate memory on device
		}
		kInfo_host->kStart = i;//defines the starting kValue for the kernel
		structToArray(kInfo_host, &param_host);//squash for transfer to device
		//DEBUGGING
		//for (int j = 0; j < structSize; j++) {
		//	tempString = (char *)calloc(3, sizeof(ktype));
		//	//writeToLog(itoa(int(param_host[j]), tempString, 10));
		//	sprintf(tempString,"%llu", param_host[j]);
		//	writeToLog(tempString);
		//}
		//exit(0);
		//END DEBUGGING
		reportCudaError(cudaMemcpy(param_device, param_host, structByteSize, cudaMemcpyHostToDevice)); //copy over kInfo
		writeToLog("Kernel Execute");
		kValid <<<B, T>>>(param_device, device_values);//execute kernel
		reportCudaError(cudaDeviceSynchronize());//wait till current computations are done. This is done in order to properly retrieve memory
		writeToLog("Kernel Complete");
		bitsUsed += (B*T);
	}
	reportCudaError(cudaMemcpy(host_values, device_values, usableMem, cudaMemcpyDeviceToHost));
	writeToFile(host_values, usableMem, kInfo_host->kRoot, end); //dump results into file
	
	//deallocate device memory;
	reportCudaError(cudaFree(device_values));
	
	//deallocate Host memory;
	free(host_values);
	free(param_host);
	
	//Close Files
	fclose(dataFile);
	fclose(logFile);

	//convert binary output into files for bill
	convertFile();

	printf("Let the Battle Begin!\n");
	printf("Press ENTER key to Continue\n");
	getchar();

	exit(0);
}