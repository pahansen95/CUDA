//for use with cgo

typedef unsigned long long int uint64;
typedef unsigned char uchar;
typedef unsigned int uint;

#ifdef __cplusplus
extern "C" {
#endif

void __declspec ( dllimport ) kernel_rangeValid(uint blocks, uint threads,
												uint64 kStart, uint64 kEnd, uchar nodes, uchar edges, uchar kSize, 
												uint64 *glob, uint64 globCap, uint64 globLength,
												uint64 *scraps, uint64 scrapsLength,
												uchar exitFlag, uint64 exitK );

#ifdef __cplusplus
}
#endif