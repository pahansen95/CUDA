//for use with cgo

typedef unsigned long long int ktype;
typedef unsigned char glob;

#ifdef __cplusplus
extern "C" {
#endif

void kernel_kValid(unsigned long int , unsigned long int , ktype *, glob *, char *);

#ifdef __cplusplus
}
#endif
