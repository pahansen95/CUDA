//For use with NVCC

typedef unsigned long long int ktype;
typedef unsigned char glob;

/*
function Declarations
*/
#ifdef __cplusplus
extern "C" {
#endif

void kernel_kValid(unsigned long int , unsigned long int , ktype *, glob *, char *);

#ifdef __cplusplus
}
#endif
/*
Kernel Declarations
*/
//__global__ void kValid(ktype *, glob *);
